library(dplyr)
library(bigrquery)
library(ggplot2)
library(ggthemes)
library(R.utils)
library(plotly)
library(data.table)
library(reshape2)

project = "ls-analytics-1358" 
bigrquery::set_service_token(service_token = "avuets.json" )

# gunzip('ali.csv','ali_orders.csv') #unzip data

read_data_advanced <- function(){
    df <- do.call(rbind, lapply(dir(pattern = "*.csv"), 
                                fread, stringsAsFactors = F))
    return(df)
}

if_less_0 <- function(x){
  x <- if_else(x<0,0,x)
  return(x)
}

all_orders <- read_data_advanced()

#saveRDS(all_orders,'all_orders.RDS')

# all_code_activate <-  query_exec(
#   "
# SELECT
#  bc_us.uid 
# 
# FROM original.bonus_code_users as bc_us
# 
# inner join (
#                            SELECT 
#                            bcid
#                           ,code
#                            FROM `ls-analytics-1358.original.bonus_code` 
#                            where code in ('Aliexpress_2Х','3XAliexpress','X3_Aliexpress','X4-Aliexpress','1.5X_Aliexpress','2X_Aliexpress','3X_Aliexpress')
#                            group by bcid ,code
#                            
#                           ) as code on bc_us.bcid = code.bcid 
#                           inner join temp.users as us on bc_us.uid = us.uid  
# where status = '1' and country = 'RU'
#   "
#   , project, use_legacy_sql = F,max_pages = Inf)




  
#descriptive statistics

all_orders <- all_orders %>% mutate_at(.vars = c("register_date","action_at"),.funs = as.Date)
all_orders <- all_orders %>% mutate(
  period = 
  case_when(
    action_at < '2019-01-18' ~ 0 ,
    action_at > '2019-02-28' ~ 2 ,
    TRUE ~ 1
    )) %>% 
    filter(action_at < '2019-04-12', action_at > '2018-12-08')   %>% 
  as.data.table() 

# old_1 <- Sys.time()
# agg_data <- 
#   all_orders %>%
#   group_by(uid_reg) %>% 
#   summarise(
#     lt_0 = round(((max(action_at[period == 0])- min(action_at[period == 0]))/n_distinct(action_at[period == 0 ]) -1),2),
#     lt_1 = round(((max(action_at[period == 1])- min(action_at[period == 1]))/n_distinct(action_at[period == 1 ]) -1),2),
#     lt_2 = round(((max(action_at[period == 2])- min(action_at[period == 2]))/n_distinct(action_at[period == 2 ]) -1),2),
#                      pu_0 = n_distinct(oid[period == 0]),
#                  pu_1 = n_distinct(oid[period == 1]),
#                  pu_2 = n_distinct(oid[period == 2])
#       )
# new_1 <- Sys.time()
# difftime(new_1, old_1, units = "secs")

old_2 <- Sys.time()
x_agg <- all_orders[,
           .(
                 lt_0 = round(as.numeric(((max(action_at[period == 0])- min(action_at[period == 0]))/n_distinct(action_at[period == 0 ]) -1)),2),
                 lt_1 = round(as.numeric(((max(action_at[period == 1])- min(action_at[period == 1]))/n_distinct(action_at[period == 1 ]) -1)),2),
                 lt_2 = round(as.numeric(((max(action_at[period == 2])- min(action_at[period == 2]))/n_distinct(action_at[period == 2 ]) -1)),2),
                 pu_d0 = n_distinct(action_at[period == 0]),
                 pu_d1 = n_distinct(action_at[period == 1]),
                 pu_d2 = n_distinct(action_at[period == 2]),
                 pu_0 = n_distinct(oid[period == 0]),
                 pu_1 = n_distinct(oid[period == 1]),
                 pu_2 = n_distinct(oid[period == 2]),
                 gmv_0 = sum(cart_rub[period == 0],na.rm = T),
                 gmv_1 = sum(cart_rub[period == 1],na.rm = T),
                 gmv_2 = sum(cart_rub[period == 2],na.rm = T),                
                 brt_0 = sum(brutto_rub[period == 0],na.rm = T),
                 brt_1 = sum(brutto_rub[period == 1],na.rm = T),
                 brt_2 = sum(brutto_rub[period == 2],na.rm = T),
                 prf_0 = sum(profit_rub[period == 0],na.rm = T),
                 prf_1 = sum(profit_rub[period == 1],na.rm = T),
                 prf_2 = sum(profit_rub[period == 2],na.rm = T), 
                 ltc_0 = n_distinct(letycode[period == 0],na.rm = T),
                 ltc_1 = n_distinct(letycode[period == 1],na.rm = T),
                 ltc_2 = n_distinct(letycode[period == 2],na.rm = T),
                 ltc_a = n_distinct(code,na.rm = T)
           ),
           uid_reg
           ]
new_2 <- Sys.time()
difftime(new_2, old_2, units = "secs")


users <- all_orders %>% filter(uid_reg %in% c(12264,14958)) 

x_agg <- x_agg %>% mutate_at(.vars = c("lt_0","lt_1","lt_2"),.funs = if_less_0)

x_agg <- as.data.table(x_agg)


x_agg <- as.data.frame(x_agg)

set.seed(123)
# 
# idx <- sample(1:NROW(x_agg),round(nrow(x_agg)/2))
# 
# x_1 <- x_agg[idx,-1]
# x_1[is.na(x_1)] <- 0
# x_2 <- x_agg[-idx,-1]
# x_2[is.na(x_2)] <- 0
# rng<-2:20 #K from 2 to 20
# tries <-50 #Run the K Means algorithm 100 times
# avg.totw.ss <-integer(length(rng)) #Set up an empty vector to hold all of points
# for(v in rng){ # For each value of the range variable
#   v.totw.ss <-integer(tries) #Set up an empty vector to hold the 100 tries
#   for(i in 1:tries){
#     k.temp <-kmeans(x_1,centers=v) #Run kmeans
#     v.totw.ss[i] <-k.temp$tot.withinss#Store the total withinss
#   }
#   avg.totw.ss[v-1] <-mean(v.totw.ss) #Average the 100 total withinss
# }
# plot(rng,avg.totw.ss,type="b", main="Total Within SS by Various K",
#      ylab="Average Total Within Sum of Squares",
#      xlab="Value of K")

x_agg[is.na(x_agg)] <- 0
cluster <- kmeans(x_agg[,-1],centers=10)
x_agg$cluster <- NULL
x_agg <- cbind(x_agg,cluster$cluster)
colnames(x_agg)[24] <- "cluster"
colnames(x_agg)[21] <- "ltc_1"

x_agg_cluster <- x_agg %>% 
  group_by(cluster) %>% 
  summarise(
         volume   = n_distinct(uid_reg),
         ltc_a    = round(mean(ltc_a),2),
         avg_lt_0 = round(mean(lt_0),2),
         avg_lt_1 = round(mean(lt_1),2),
         avg_lt_2 = round(mean(lt_2),2),
         avg_cart_0 = round(sum(gmv_0, na.rm = T)/sum(pu_0),2),
         avg_cart_1 = round(sum(gmv_1, na.rm = T)/sum(pu_1),2),
         avg_cart_2 = round(sum(gmv_2, na.rm = T)/sum(pu_2),2),
         cart_0     = sum(gmv_0, na.rm = T),
         cart_1     = sum(gmv_1, na.rm = T),
         cart_2     = sum(gmv_2, na.rm = T),
         brt_0     = sum(brt_0, na.rm = T),
         brt_1     = sum(brt_1, na.rm = T),
         brt_2     = sum(brt_2, na.rm = T),
         prf_0     = sum(prf_0, na.rm = T),
         prf_1     = sum(prf_1, na.rm = T),
         prf_2     = sum(prf_2, na.rm = T),
         avg_pu_pd_0 = round(sum(pu_0)/sum(pu_d0),2),
         avg_pu_pd_1 = round(sum(pu_1)/sum(pu_d1),2),
         avg_pu_pd_2 = round(sum(pu_2)/sum(pu_d2),2)
  ) %>% 
  mutate(
    cart_0_f = round(cart_0/ sum(cart_0)*100,2),
    cart_1_f = round(cart_1/ sum(cart_1)*100,2),    
    cart_2_f = round(cart_2/ sum(cart_2)*100,2) 
         
         ) %>% 
  as.data.table()

# x <- merge(x,x_agg[,c(1,24)], by = 'uid_reg', all.x = T)
# x <- merge(x,churn_status, by = 'uid_reg', all.x = T)
# 
# x$activity_status_b <- ifelse(is.na(x$activity_status_b),'new',x$activity_status_b)
# x$activity_status_a <- ifelse(is.na(x$activity_status_a),'new',x$activity_status_a)

#descriptive cluster
all_orders_cluster <- all_orders %>% 
                    inner_join(select(x_agg, uid_reg, cluster), by = "uid_reg") %>% 
                    as.data.table()

conv <- 
  all_orders_cluster[order(cluster),
           .(
            volume = n_distinct(uid_reg),
            conv_0 = n_distinct(uid_reg[period == 0]),
            conv_1 = n_distinct(uid_reg[period == 1]),
            conv_2 = n_distinct(uid_reg[period == 2])
           ),
           cluster
           ]


#churn fraction dynamic
churn <- 
cbind(
x %>% 
  group_by(activity_status_b) %>%  
  summarise(n = n_distinct(uid_reg)) %>% 
  mutate(n_f_b = round(n/ sum(n)*100,2)) %>% select(- n) %>% 
  arrange(activity_status_b)
  ,

x %>% 
  group_by(activity_status_a) %>%  
  summarise(n = n_distinct(uid_reg)) %>% 
  mutate(n_f_a = round(n/ sum(n)*100,2)) %>%
  arrange(activity_status_a) %>% select(- n,-activity_status_a)

) %>% 
  group_by(activity_status_b) %>% 
  mutate(delta = n_f_a - n_f_b)


#cluster economic
economic <- 
cluster_agg %>% 
  group_by(cluster,volume,avg_cart_0,avg_cart_2,cart_2) %>% 
  summarise(
        delta_avg_cart = sum(avg_cart_2) - sum(avg_cart_0),
        pu_2 = round(cart_2/avg_cart_2),
        brutto_2 = round(sum(brt_2) / sum(cart_2),4),
        profit_2 = round(sum(prf_2) / sum(cart_2),4),
        profit   = sum(prf_2)
    
  ) %>% 
  mutate(
    cart_if_0  = round(avg_cart_0 * pu_2,2),
    delta_cart = round(sum(cart_2) - (cart_if_0),2)
    
  ) %>% ungroup() %>%  summarise(sum(delta_cart))


